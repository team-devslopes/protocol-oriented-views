//
//  LoginVC.swift
//  ProtocolSuperAwesomeTime
//
//  Created by Caleb Stultz on 9/14/16.
//  Copyright © 2016 Caleb Stultz. All rights reserved.
//

import UIKit

class DataEntryTextField: UITextField, Jitterable {
    
}

class LoginButton: UIButton, Jitterable {
    
}

class FlashingLabel: UILabel, Flashable, Jitterable {
    
}

class LoginVC: UIViewController {

    @IBOutlet weak var emailField: DataEntryTextField!
    @IBOutlet weak var passwordField: DataEntryTextField!
    @IBOutlet weak var errorLabel: FlashingLabel!
    @IBOutlet weak var loginBtn: LoginButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func loginBtnWasPressed(_ sender: AnyObject) {
        emailField.jitter()
        passwordField.jitter()
        loginBtn.jitter()
        errorLabel.flash()
        errorLabel.jitter()
    }
    

}

