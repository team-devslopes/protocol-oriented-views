# Slope Session - iOS Swift 3 Beginners Tutorial: Protocol-Oriented Views - Build An Animation Library in Swift 3 #

[![protocol-oriented-views-animations.png](https://bitbucket.org/repo/yLGanB/images/775367986-protocol-oriented-views-animations.png)
](https://youtu.be/AySlYrel7fc)

*Click the image above to watch this Slope Session on YouTube.*
### Description ###

This is the source code accompanying the *iOS Swift 3 Beginners Tutorial: Protocol-Oriented Views - Build An Animation Library in Swift 3* Slope Session. You will build an animation library using protocols that can be easily read and passed around your code base. 👍

**Note:** *Be sure to drag in all images from the 'Resources' folder and into your Assets.xcassets in Xcode so that the app can successfully find and use them.*

Happy coding!

![170.png](https://bitbucket.org/repo/9LeroX/images/1216100977-170.png)

**Caleb Stultz**